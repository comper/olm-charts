# OLM Charts

OLM Charts is a free and open-source JavaScript library for visualizing [COMPER](https://comper.fr/en/) Open Learner Model profiles.

## Getting Started

### Dependencies

The project uses [D3](https://d3js.org/) and [requireJS](https://requirejs.org/), as well
as [Bootstrap](https://getbootstrap.com/) and [OverlayScrollbars](https://github.com/KingSora/OverlayScrollbars) for its
demos.

### Installation

Execute the following commands to bundle the library:

```shell
npm install
npm run build
```

## Usage

Once you include the resulting `olm.bundle.js`, the module will be available as `document._OLM`.

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <script type="text/javascript" src="./olm.bundle.js"></script>
    <title>OLM Visualization</title>
</head>
<body>

</body>
<script>
    (function () {
        const OLM = document._OLM;

        // This creates a random COMPER profile for testing purposes.
        const framework = OLM.CORE.Utils.getScoredFrameworkSample();

        let fw_tree = new OLM.CORE.FrameworkTree();
        fw_tree.buildFromFramework(framework);

        // You can then instantiate the desired visualization.
    })();
</script>
</html>
```

### Visualizations

A demo for each visualization can be found under the `./test` folder, as well
as [online](https://lachand.gitlab.io/open-learner-model/).

#### Tree Indented

To instantiate this visualization, the following configuration object (presented here with default values) must be
provided to the constructor:

```json
{
  "fontHoverColor": "rgba(255, 255, 255, 1)",
  "fontColor": "rgba(255, 255, 255, .85)",
  "colors": [
    {
      "to": 0.25,
      "color": "#cf000f"
    },
    {
      "to": 0.5,
      "color": "#f57f17"
    },
    {
      "to": 0.75,
      "color": "#ffee58"
    },
    {
      "color": "#4caf50"
    }
  ],
  "showMastery": true,
  "showTrust": true,
  "showCover": true,
  "formatMastery": "percentage",
  "formatTrust": "percentage",
  "formatCover": "percentage"
}
```

Available values for `formatMastery` are `percentage` and `2decimal`.

```javascript
const treeIndented = new OLM.TreeIndented(document.getElementById('test'), fw_tree, config);
treeIndented.onMouseOver = (node) => {
    //
}
treeIndented.draw(svgId = 'test-indented');
// If svgId is omitted, a unique ID will be automatically created.
```

#### Tree Pack / Tree Partition / Tree Sunburst

To instantiate these visualizations, the following configuration object (presented here with default values) must be
provided to the constructor:

```json
{
  "fontColor": "rgba(255, 255, 255, .85)",
  "backgroundColor": "#343a40",
  "formatMastery": "percentage",
  "formatTrust": "percentage",
  "formatCover": "percentage",
  "useHash": true,
  "hashTreshold": 0.1,
  "useLegend": true,
  "colors": [
    {
      "to": 0.25,
      "color": "#cf000f"
    },
    {
      "to": 0.5,
      "color": "#f57f17"
    },
    {
      "to": 0.75,
      "color": "#ffee58"
    },
    {
      "color": "#4caf50"
    }
  ],
  "noValueColor": "#808080"
}
```

Available values for `formatMastery` are `percentage` and `2decimal`. Moreover, `backgroundColor` only changes the
background color of the information overlay.

```javascript
const treePack = new OLM.TreePack(document.getElementById('test'), fw_tree, config);
treePack.onMouseOver = (node) => {
    //
}
treePack.draw(svgId = 'test-pack');
// If svgId is omitted, a unique ID will be automatically created.
```

## Acknowledgments

Thanks to [@stopyransky](https://github.com/stopyransky) for
the [D3 Hierarchy Layouts](https://codepen.io/stopyransky/pen/EXdrOo).
