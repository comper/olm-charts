import {Core} from "./core/core"
import {TreeIndented} from "./tree_indented/tree_indented";
import {TreePack} from "./tree_pack/tree_pack";
import {TreePartition} from "./tree_partition/tree_partition";
import {TreeSunburst} from "./tree_sunburst/tree_sunburst";

class OLM {

    public static CORE = Core;
    public static TreePack = TreePack;
    public static TreeSunburst = TreeSunburst;
    public static TreePartition = TreePartition;
    public static TreeIndented = TreeIndented;
}

(<any>document)._OLM = OLM;
