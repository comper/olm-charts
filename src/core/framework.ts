import {Utils} from "./utils";

interface FrameworkNodeRelation {
    hasSkill: Array<string>,
    isSkillOf: Array<string>,
    hasKnowledge: Array<string>,
    isKnowledgeOf: Array<string>,
    composes: Array<string>,
    isComposedOf: Array<string>,
    complexifies?: Array<string>,
    isComplexificationOf?: Array<string>,
    isLeverOfUnderstandingOf?: Array<string>,
    facilitatesUnderstandingOf?: Array<string>,
    comprises?: Array<string>,
    isComprisedIn?: Array<string>,
    hasLearning?: Array<string>,
    hasTraining?: Array<string>,
    requires?: Array<string>,
    isRequiredBy?: Array<string>,

    [key: string]: Array<string>
}

interface FrameworkNode {
    name: string,
    type: string,
    children?: Array<string>,
    parents?: Array<string>,
    cover?: number,
    mastery?: number,
    trust?: number,
    resourcesCompleted?: number,
    relations?: FrameworkNodeRelation,
    resources?: Array<string>

    [key: string]: (string | FrameworkNodeRelation | Array<string> | number)
}

interface FrameworkRoot {
    name: string,
    type: string
}

interface Framework {
    name: string,
    objects: Array<FrameworkNode>
    resources: Array<FrameworkResources>
}

interface FrameworkTreeNode {
    id: string,                             // Unique ID
    data: FrameworkNode,                      // The node data
    parent: string,                             // ID of the parent
    children: Array<string>,                      // IDs of the children
    depth: number,                              // depth of the node
    resources: Array<FrameworkResources>,

    [key: string]: (string | FrameworkNode | Array<string> | number | Array<FrameworkResources>);
}

interface FrameworkResources {
    id: string,
    name: string,
    interactivityType: string,
    learningResourceType: string,
    significanceLevel: number,
    difficulty: number,
    typicalLearningTime: number,
    learningPlatform: string
    location: string,
    author: string,
    language: string,
    generative: boolean
    //[key :string] :(string | FrameworkNode | Array<string> | number);
}

class FrameworkTree {

    /**
     * Root of framework tree
     */
    private root: FrameworkTreeNode = null;

    /**
     * Map of the framework nodes tree with their own id.
     */
    private nodes: Map<string, FrameworkTreeNode> = new Map<string, FrameworkTreeNode>();

    consextructor() {
    }

    static copy(source: FrameworkTree): FrameworkTree {
        let copy = new FrameworkTree();
        copy.nodes = new Map(source.nodes);
        copy.root = source.root;
        return copy;
    }

    /**
     * Gets the parent node at the given depth.
     * @param node
     * @param depth
     */
    public getNodeParentByDepth(node: FrameworkTreeNode, depth: number): FrameworkTreeNode {
        while (node.depth > depth) {
            node = this.nodes.get(node.parent);
        }
        return node;
    }

    /**
     * Builds the tree from a framework.
     * @param framework
     */
    public buildFromFramework(framework: Framework): void {
        this.root = {
            "id": Utils.makeId(),
            "data": {
                "name": framework.name,
                "type": "framework"
            },
            "parent": null,
            "children": [],
            "depth": 0,
            "resources": [],
        }
        this.nodes.set(this.root.id, this.root);

        // Copy of the framework to edit it without editing the source
        let fw: Array<FrameworkNode> = Array.from(framework.objects);
        // Increment var for the while loop below
        let i: number = 0;
        // Map <name, id>. Helps retrieve the FrameworkTreeNode.id from the corresponding node.name.
        let createdNodes: Map<string, string> = new Map<string, string>();

        // Creates a new FrameworkTreeNode for each node<->parentNode link.
        // Once every node<->parentNode link as been treated, removes the node from fw.
        // Loops until there are no nodes in fw anymore.
        while (fw.length > 0) {
            // "increment" will be False if we removed an element for fw, because 'i' will be on the new current node position.
            let increment: boolean = true;
            let node: FrameworkNode = fw[i];
            let parents: Array<string> = [node.relations.isSkillOf, node.relations.isKnowledgeOf, node.relations.composes].flat();
            const useFilter = (arr: any[]) => {
                return arr.filter((value, index, self) => {
                    return self.indexOf(value) === index;
                });
            };
            let resources = [node.relations.hasTraining, node.relations.hasLearning].flat();
            let resourcesF: Array<FrameworkResources> = [];
            for (let i = 0; i < resources.length; i++) {
                for (let resourceFramework of framework.resources) {
                    if (resources[i] === resourceFramework["id"]) {
                        resourcesF[i] = resourceFramework;
                    }
                }
            }
            parents = useFilter(parents);

            // For the first level nodes, we simply link them to the root of the tree.
            if (parents.length === 0) {
                let id: string = Utils.makeId();
                let treeNode: FrameworkTreeNode = {
                    "id": id,
                    "data": node,
                    "parent": this.root.id,
                    "children": [],
                    "depth": 1,
                    "resources": resourcesF,
                }
                this.nodes.set(id, treeNode);
                this.root.children.push(id);
                createdNodes.set(node.name, id);
                fw.splice(i, 1);
                increment = false;
            } else {
                // Adds the "linkedParents" names to the node, so if we have to loop again on the node, we know which 
                // node<->parentNode link has been traited or not.
                if (node['linkedParents'] === undefined) node['linkedParents'] = [];
                parents.forEach((parentName: string) => {
                    if (!(node['linkedParents'] as Array<string>).includes(parentName)) {
                        let parentId: string = createdNodes.get(parentName);
                        if (parentId !== undefined) {
                            let resources = [node.relations.hasTraining, node.relations.hasLearning].flat();
                            let resourcesF: Array<FrameworkResources> = [];
                            for (let i = 0; i < resources.length; i++) {
                                for (let resourceFramework of framework.resources) {
                                    if (resources[i] === resourceFramework["id"]) {
                                        resourcesF[i] = resourceFramework;
                                    }
                                }
                            }
                            let parent: FrameworkTreeNode = this.nodes.get(parentId);
                            let id: string = Utils.makeId()
                            let treeNode: FrameworkTreeNode = {
                                "id": id,
                                "data": node,
                                "parent": parentId,
                                "children": [],
                                "depth": parent.depth + 1,
                                "resources": resourcesF,
                            }
                            this.nodes.set(id, treeNode);
                            parent.children.push(id);
                            createdNodes.set(node.name, id);
                            (node['linkedParents'] as Array<string>).push(parentName);
                        }
                    }
                });

                // If we traited all the node<->parentNode links, we can remove the node from fw.
                if ((node['linkedParents'] as Array<string>).length === parents.length) {
                    fw.splice(i, 1);
                    increment = false;
                }
            }
            // Increments or resets until we're finished with the nodes.
            if (increment) i += 1;
            if (i >= fw.length) i = 0;
        }
    }

    /**
     * Gets root
     * @returns root
     */
    public getRoot(): FrameworkTreeNode {
        return this.root;
    }

    /**
     * Gets a framework tree node of the tree.
     * @param id Id of the framework tree node.
     * @returns node A framework tree node.
     */
    public getNode(id: string): FrameworkTreeNode {
        return this.nodes.get(id);
    }

    /**
     * Returns the nodes of the tree as array.
     * @returns
     */
    public getNodes() {
        return Array.from(this.nodes.values());
    }

    /**
     * Returns the nearest common ancestor of two given nodes.
     * @param node1
     * @param node2
     * @returns nearest common ancestor
     */
    public getNearestCommonAncestor(node1: FrameworkTreeNode, node2: FrameworkTreeNode): FrameworkTreeNode {
        while (node1.depth > node2.depth) {
            node1 = this.getNode(node1.parent);
        }
        while (node2.depth > node1.depth) {
            node2 = this.getNode(node2.parent);
        }
        while (node1.id !== node2.id) {
            node1 = this.getNode(node1.parent);
            node2 = this.getNode(node2.parent);
        }
        return node1;
    }


    public getNodesByDepth(depth: number): Array<FrameworkTreeNode> {
        let treeNodes: Map<string, FrameworkTreeNode> = this.nodes;
        let resultNodes: Array<FrameworkTreeNode> = [];

        (function recurse(currentNode) {
            if (currentNode.depth < depth) {
                for (var i = 0, length = currentNode.children.length; i < length; i++) {
                    recurse(treeNodes.get(currentNode.children[i]));
                }
            }
            if (currentNode.depth === depth) resultNodes.push(currentNode);
        })(this.root);

        return resultNodes;
    }

    /**
     * Traverses the tree depth first, starting from origin. Calls callback at each node, with the node as parameter.
     * @param callback
     * @param origin Default : the root of the tree
     */
    public traverseDeepFirst(callback: (node: FrameworkTreeNode) => void, origin: FrameworkTreeNode = this.root): void {
        let nodes: Map<string, FrameworkTreeNode> = this.nodes;
        (function recurse(currentNode) {
            for (var i = 0, length = currentNode.children.length; i < length; i++) {
                recurse(nodes.get(currentNode.children[i]));
            }
            callback(currentNode);
        })(origin);
    };

    /**
     * Finds nodes
     * @param values Rest tuple of a value and a path. The path is a single string separated by a dot.
     * @returns nodes An array of framework tree nodes.
     * @example let nodes = tree.findNode([1, "cover"], ["some_name", "relations.requires"])
     */
    public findNodes(...values: Array<[(string | number), string]>): Array<FrameworkTreeNode> {
        function hasData(node: FrameworkTreeNode, values: Array<[(string | number), string]>): boolean {
            if (node.data === null || node.data === undefined) return false;
            for (let i = 0; i < values.length; i++) {
                let value: (string | number) = values[i][0];
                let path: Array<string> = values[i][1].split('.');
                let data: (string | FrameworkNodeRelation | number | Array<(string | number)>) = node.data[path[0]];
                if (data === undefined) return false;
                // Follows the path.
                for (let j = 1; j < path.length; j++) {
                    if ((data as FrameworkNodeRelation)[path[j]] === undefined) return false;
                    ;
                    data = (data as FrameworkNodeRelation)[path[j]];
                }
                // Checks weither the value is in a list of values or equal to the value.
                if ((Array.isArray(data) && !data.includes(value)) || (!Array.isArray(data) && data !== value)) {
                    return false;
                }
            }
            return true;
        }

        let nodes: Array<FrameworkTreeNode> = [];
        let fwNodes: Array<FrameworkTreeNode> = Array.from(this.nodes.values());
        for (let j = 0; j < fwNodes.length; j++) {
            let node: FrameworkTreeNode = fwNodes[j];
            if (hasData(node, values)) nodes.push(node);
        }
        return nodes;
    }

    /**
     * Gets the siblings of a framework tree node.
     * @param id Id of the framework tree node.
     * @returns siblings An array of framework tree node.
     */
    public getSiblings(id: string): Array<FrameworkTreeNode> {
        let node: FrameworkTreeNode = this.nodes.get(id);
        let parent: FrameworkTreeNode = this.nodes.get(node.parent);
        let siblings: Array<FrameworkTreeNode> = [];
        parent.children.forEach((childId: string) => {
            siblings.push(this.nodes.get(childId));
        })
        return siblings;
    }
}

export {FrameworkNode, Framework, FrameworkTreeNode, FrameworkTree, FrameworkRoot};
