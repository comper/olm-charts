import {Utils} from "./utils";
import {FrameworkTree} from "./framework";

export class Core {

    public static Utils = Utils;
    public static FrameworkTree = FrameworkTree;
}
