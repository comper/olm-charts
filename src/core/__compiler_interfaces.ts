interface JsTree {
    settings: any;

    refresh(): void;
}

interface JQuery {
    jstree(any: any): JsTree;
}
