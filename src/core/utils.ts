import {Framework, FrameworkNode} from "./framework";

/*
* OLM tools, all functions are static.
*/
class Utils {

    /**
     * @returns {{objects : Array<FrameworkNode>}}, a test framework from real application.
     */
    public static getFrameworkSample(): Framework {
        return {
            "name": "LIFAP2",
            "objects": [
                {
                    "name": "rec_prof_liste_donne_liste",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "Savoir_\\u00e9crire_une_fonction_r\\u00e9cursive_sur_des_listes_qui_op\\u00e8re_en_profondeur"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "rec_liste_donne_liste"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "\\u00e9valuer_une_expression_Scheme",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [
                            "Savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9pondant_\\u00e0_une_sp\\u00e9cification"
                        ],
                        "isKnowledgeOf": [],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_les_fonctions_pr\\u00e9d\\u00e9finies",
                            "conna\\u00eetre_le_processus_d_\\u00e9valuation_de_l_appel_\\u00e0_une_fonction"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "non_rec_liste_donne_liste",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_non_r\\u00e9cursive_sur_des_listes"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_les_fonctions_sur_les_listes"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Savoir_parcourir_un_arbre_binaire",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [
                            "Utiliser_les_TDA_liste_et_arbre"
                        ],
                        "isKnowledgeOf": [],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "Conna\\u00eetre_la_d\\u00e9finition_d_un_arbre_binaire"
                        ],
                        "hasLearning": [],
                        "hasTraining": [
                            "asker:26",
                            "asker:28",
                            "asker:27"
                        ]
                    }
                },
                {
                    "name": "rec_prof_liste_donne_nb",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "Savoir_\\u00e9crire_une_fonction_r\\u00e9cursive_sur_des_listes_qui_op\\u00e8re_en_profondeur"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "rec_liste_donne_nb"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "rec_prof_liste_donne_liste_de_2_r\\u00e9sultats",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "Savoir_\\u00e9crire_une_fonction_r\\u00e9cursive_sur_des_listes_qui_op\\u00e8re_en_profondeur"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "rec_liste_donne_liste_de_2_r\\u00e9sultats"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Savoir_\\u00e9crire_une_fonction_r\\u00e9cursive_en_remontant_ou_descendant",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [
                            "Savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9pondant_\\u00e0_une_sp\\u00e9cification"
                        ],
                        "isKnowledgeOf": [],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "Savoir_reconna\\u00eetre_si_une_fonction_r\\u00e9cursive_effectue_les_calculs_en_remontant_ou_en_descendant"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "savoir_utiliser_let_pour_m\\u00e9moriser_une_valeur",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [
                            "Savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9pondant_\\u00e0_une_sp\\u00e9cification"
                        ],
                        "isKnowledgeOf": [],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_le_fonctionnement_du_let",
                            "savoir_reconna\\u00eetre_si_le_let_est_indispensable_ou_non"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "rec_liste_donne_nb",
                    "type": "Skills",
                    "mastery": 1,
                    "trust": 1,
                    "cover": 1,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9cursive_sur_des_listes"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "non_rec_liste_donne_nb"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "non_rec_arbre_donne_nb",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_non_r\\u00e9cursive_sur_des_arbres"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_les_fonctions_de_calcul",
                            "conna\\u00eetre_les_primitives_sur_les_arbres"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "non_rec_nb_donne_nb",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_non_r\\u00e9cursive_sur_des_nombres"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_les_fonctions_de_calcul"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "non_rec_liste_donne_bool\\u00e9en",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_non_r\\u00e9cursive_sur_des_listes"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_les_fonctions_sur_les_listes"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "non_rec_nb_donne_bool\\u00e9en",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_non_r\\u00e9cursive_sur_des_nombres"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_les_fonctions_de_calcul"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "rec_liste_donne_liste_de_2_r\\u00e9sultats",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9cursive_sur_des_listes"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "Savoir_\\u00e9crire_une_fonction_r\\u00e9cursive_en_remontant_ou_descendant",
                            "savoir_utiliser_let_pour_m\\u00e9moriser_une_valeur"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "non_rec_arbre_donne_arbre",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_non_r\\u00e9cursive_sur_des_arbres"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_les_primitives_sur_les_arbres"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "rec_liste_donne_liste",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9cursive_sur_des_listes"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "non_rec_liste_donne_liste"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9cursive_sur_des_listes",
                    "type": "Skills",
                    "mastery": 0.2,
                    "trust": 0.2,
                    "cover": 0.2,
                    "relations": {
                        "isSkillOf": [
                            "Savoir_\\u00e9crire_une_fonction_r\\u00e9cursive_en_Scheme"
                        ],
                        "isKnowledgeOf": [],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [
                            "Savoir_\\u00e9crire_une_fonction_r\\u00e9cursive_sur_des_listes_qui_op\\u00e8re_en_profondeur",
                            "rec_liste_donne_liste_de_2_r\\u00e9sultats",
                            "rec_liste_donne_liste",
                            "rec_liste_donne_bool\\u00e9en",
                            "rec_liste_donne_nb"
                        ],
                        "requires": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_non_r\\u00e9cursive_sur_des_listes",
                            "Savoir_parcourir_une_liste"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "rec_liste_donne_bool\\u00e9en",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9cursive_sur_des_listes"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "non_rec_liste_donne_bool\\u00e9en"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "rec_arbre_donne_liste",
                    "type": "Skills",
                    "mastery": 0.8,
                    "trust": 1,
                    "cover": 1,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9cursive_sur_des_arbres"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_les_fonctions_sur_les_listes",
                            "non_rec_arbre_donne_liste",
                            "conna\\u00eetre_les_primitives_sur_les_arbres"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "savoir_reconna\\u00eetre_si_le_let_est_indispensable_ou_non",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [
                            "Savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9pondant_\\u00e0_une_sp\\u00e9cification"
                        ],
                        "isKnowledgeOf": [],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": [
                            "asker:102"
                        ]
                    }
                },
                {
                    "name": "savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9cursive_sur_des_nombres",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [
                            "Savoir_\\u00e9crire_une_fonction_r\\u00e9cursive_en_Scheme"
                        ],
                        "isKnowledgeOf": [],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [
                            "rec_nb_donne_nb",
                            "rec_nb_donne_bool\\u00e9en"
                        ],
                        "requires": [
                            "conna\\u00eetre_les_fonctions_de_calcul",
                            "savoir_\\u00e9crire_une_fonction_Scheme_non_r\\u00e9cursive_sur_des_nombres"
                        ],
                        "hasLearning": [],
                        "hasTraining": [
                            "asker:19",
                            "asker:172"
                        ]
                    }
                },
                {
                    "name": "rec_arbre_donne_bool\\u00e9en",
                    "type": "Skills",
                    "mastery": 0.4,
                    "trust": 1,
                    "cover": 1,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9cursive_sur_des_arbres"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "non_rec_arbre_donne_bool\\u00e9en",
                            "conna\\u00eetre_les_primitives_sur_les_arbres"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "savoir_\\u00e9crire_une_fonction_Scheme_non_r\\u00e9cursive_sur_des_arbres",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [
                            "Savoir_\\u00e9crire_une_fonction_non_r\\u00e9cursive_en_Scheme"
                        ],
                        "isKnowledgeOf": [],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [
                            "non_rec_arbre_donne_arbre",
                            "non_rec_arbre_donne_bool\\u00e9en",
                            "non_rec_arbre_donne_liste",
                            "non_rec_arbre_donne_nb"
                        ],
                        "requires": [
                            "conna\\u00eetre_les_primitives_sur_les_arbres"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Savoir_reconna\\u00eetre_si_une_fonction_r\\u00e9cursive_effectue_les_calculs_en_remontant_ou_en_descendant",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [
                            "Savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9pondant_\\u00e0_une_sp\\u00e9cification"
                        ],
                        "isKnowledgeOf": [],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": [
                            "asker:103",
                            "asker:286"
                        ]
                    }
                },
                {
                    "name": "rec_prof_liste_donne_bool\\u00e9en",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "Savoir_\\u00e9crire_une_fonction_r\\u00e9cursive_sur_des_listes_qui_op\\u00e8re_en_profondeur"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "rec_liste_donne_bool\\u00e9en"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "savoir_\\u00e9crire_une_fonction_Scheme_non_r\\u00e9cursive_sur_des_nombres",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [
                            "Savoir_\\u00e9crire_une_fonction_non_r\\u00e9cursive_en_Scheme"
                        ],
                        "isKnowledgeOf": [],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [
                            "non_rec_nb_donne_bool\\u00e9en",
                            "non_rec_nb_donne_nb"
                        ],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": [
                            "asker:19",
                            "asker:172"
                        ]
                    }
                },
                {
                    "name": "Reconna\\u00eetre_un_algorithme_r\\u00e9cursif_ou_non",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [
                            "Comprendre_la_r\\u00e9cursivit\\u00e9"
                        ],
                        "isKnowledgeOf": [],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "Conna\\u00eetre_la_d\\u00e9finition_de_la_r\\u00e9cursivit\\u00e9"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "non_rec_liste_donne_nb",
                    "type": "Skills",
                    "mastery": 0.5,
                    "trust": 1,
                    "cover": 1,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_non_r\\u00e9cursive_sur_des_listes"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_les_fonctions_sur_les_listes",
                            "conna\\u00eetre_les_fonctions_de_calcul"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Savoir_parcourir_une_liste",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [
                            "Utiliser_les_TDA_liste_et_arbre"
                        ],
                        "isKnowledgeOf": [],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "Conna\\u00eetre_la_d\\u00e9finition_d_une_liste_cha\\u00een\\u00e9e"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "rec_arbre_donne_nb",
                    "type": "Skills",
                    "mastery": 1,
                    "trust": 1,
                    "cover": 1,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9cursive_sur_des_arbres"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_les_fonctions_de_calcul",
                            "conna\\u00eetre_les_primitives_sur_les_arbres",
                            "non_rec_arbre_donne_nb"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Savoir_\\u00e9crire_une_fonction_r\\u00e9cursive_sur_des_listes_qui_op\\u00e8re_en_profondeur",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9cursive_sur_des_listes"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [
                            "rec_prof_liste_donne_liste",
                            "rec_prof_liste_donne_bool\\u00e9en",
                            "rec_prof_liste_donne_nb",
                            "rec_prof_liste_donne_liste_de_2_r\\u00e9sultats"
                        ],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "rec_nb_donne_nb",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9cursive_sur_des_nombres"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_les_fonctions_de_calcul",
                            "non_rec_nb_donne_nb"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "savoir_\\u00e9crire_une_fonction_Scheme_non_r\\u00e9cursive_sur_des_listes",
                    "type": "Skills",
                    "mastery": 0.16666666666666666,
                    "trust": 0.3333333333333333,
                    "cover": 0.3333333333333333,
                    "relations": {
                        "isSkillOf": [
                            "Savoir_\\u00e9crire_une_fonction_non_r\\u00e9cursive_en_Scheme"
                        ],
                        "isKnowledgeOf": [],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [
                            "non_rec_liste_donne_bool\\u00e9en",
                            "non_rec_liste_donne_liste",
                            "non_rec_liste_donne_nb"
                        ],
                        "requires": [
                            "conna\\u00eetre_les_fonctions_sur_les_listes"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "non_rec_arbre_donne_bool\\u00e9en",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_non_r\\u00e9cursive_sur_des_arbres"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_les_primitives_sur_les_arbres"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "non_rec_arbre_donne_liste",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_non_r\\u00e9cursive_sur_des_arbres"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_les_fonctions_sur_les_listes",
                            "conna\\u00eetre_les_primitives_sur_les_arbres"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "rec_nb_donne_bool\\u00e9en",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9cursive_sur_des_nombres"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "non_rec_nb_donne_bool\\u00e9en",
                            "conna\\u00eetre_les_fonctions_de_calcul"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9cursive_sur_des_arbres",
                    "type": "Skills",
                    "mastery": 0.76,
                    "trust": 1,
                    "cover": 1,
                    "relations": {
                        "isSkillOf": [
                            "Savoir_\\u00e9crire_une_fonction_r\\u00e9cursive_en_Scheme"
                        ],
                        "isKnowledgeOf": [],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [
                            "rec_arbre_donne_bool\\u00e9en",
                            "rec_arbre_donne_liste",
                            "rec_arbre_donne_liste_de_2_r\\u00e9sultats",
                            "rec_arbre_donne_arbre",
                            "rec_arbre_donne_nb"
                        ],
                        "requires": [
                            "Savoir_parcourir_un_arbre_binaire",
                            "savoir_\\u00e9crire_une_fonction_Scheme_non_r\\u00e9cursive_sur_des_arbres",
                            "conna\\u00eetre_les_primitives_sur_les_arbres",
                            "conna\\u00eetre_la_repr\\u00e9sentation_d_un_arbre_binaire"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "rec_arbre_donne_liste_de_2_r\\u00e9sultats",
                    "type": "Skills",
                    "mastery": 1,
                    "trust": 1,
                    "cover": 1,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9cursive_sur_des_arbres"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_les_primitives_sur_les_arbres",
                            "Savoir_\\u00e9crire_une_fonction_r\\u00e9cursive_en_remontant_ou_descendant",
                            "savoir_utiliser_let_pour_m\\u00e9moriser_une_valeur"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Savoir_distinguer_un_ABR_d_un_arbre_non_ordonn\\u00e9",
                    "type": "Skills",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [
                            "Utiliser_les_TDA_liste_et_arbre"
                        ],
                        "isKnowledgeOf": [],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "Conna\\u00eetre_la_d\\u00e9finition_d_un_ABR"
                        ],
                        "hasLearning": [],
                        "hasTraining": [
                            "asker:35"
                        ]
                    }
                },
                {
                    "name": "rec_arbre_donne_arbre",
                    "type": "Skills",
                    "mastery": 0.6,
                    "trust": 1,
                    "cover": 1,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9cursive_sur_des_arbres"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "non_rec_arbre_donne_arbre",
                            "conna\\u00eetre_les_primitives_sur_les_arbres"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "conna\\u00eetre_les_structures_de_conditionnelle_alternative",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [
                            "Savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9pondant_\\u00e0_une_sp\\u00e9cification"
                        ],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "conna\\u00eetre_les_primitives_de_test_sur_les_listes",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_fonctions_sur_les_listes"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_la_repr\\u00e9sentation_d_une_liste"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Conna\\u00eetre_les_3_questions_\\u00e0_se_poser",
                    "type": "Knowledge",
                    "mastery": 0.4666666666666666,
                    "trust": 0.6111254543054799,
                    "cover": 1,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [
                            "Concevoir_un_algorithme_r\\u00e9cursif"
                        ],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "savoir_comment_d\\u00e9finir_l_ent\\u00eate_d_une_fonction",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [
                            "Savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9pondant_\\u00e0_une_sp\\u00e9cification"
                        ],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "conna\\u00eetre_le_fonctionnement_du_let",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [
                            "Savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9pondant_\\u00e0_une_sp\\u00e9cification"
                        ],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": [
                            "asker:417"
                        ]
                    }
                },
                {
                    "name": "conna\\u00eetre_la_fonction_car",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_primitives_d_acc\\u00e8s_aux_listes"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_la_repr\\u00e9sentation_d_une_liste"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "conna\\u00eetre_la_fonction_list",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_primitives_de_construction_de_listes"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_la_repr\\u00e9sentation_d_une_liste"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "conna\\u00eetre_define",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_formes_sp\\u00e9ciales"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "conna\\u00eetre_quote",
                    "type": "Knowledge",
                    "mastery": 1,
                    "trust": 1,
                    "cover": 1,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_formes_sp\\u00e9ciales"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": [
                            "asker:2"
                        ]
                    }
                },
                {
                    "name": "conna\\u00eetre_la_repr\\u00e9sentation_d_un_arbre_binaire",
                    "type": "Knowledge",
                    "mastery": 0.6,
                    "trust": 1,
                    "cover": 1,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_primitives_sur_les_arbres"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": [
                            "asker:4",
                            "asker:434"
                        ]
                    }
                },
                {
                    "name": "conna\\u00eetre_les_primitives_de_construction_d_arbres",
                    "type": "Knowledge",
                    "mastery": 0.4,
                    "trust": 1,
                    "cover": 1,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_primitives_sur_les_arbres"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_la_repr\\u00e9sentation_d_un_arbre_binaire"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "conna\\u00eetre_les_primitives_de_construction_de_listes",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_fonctions_sur_les_listes"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [
                            "conna\\u00eetre_la_fonction_append",
                            "conna\\u00eetre_la_fonction_cons",
                            "conna\\u00eetre_la_fonction_list"
                        ],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "conna\\u00eetre_les_primitives_de_test_sur_les_arbres",
                    "type": "Knowledge",
                    "mastery": 0.2,
                    "trust": 1,
                    "cover": 1,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_primitives_sur_les_arbres"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_la_repr\\u00e9sentation_d_un_arbre_binaire"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Conna\\u00eetre_la_d\\u00e9finition_d_un_arbre_binaire",
                    "type": "Knowledge",
                    "mastery": 0.8,
                    "trust": 1,
                    "cover": 1,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [
                            "Utiliser_les_TDA_liste_et_arbre"
                        ],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Savoir_dans_quel_ordre_se_poser_les_3_questions",
                    "type": "Knowledge",
                    "mastery": 1,
                    "trust": 1,
                    "cover": 1,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [
                            "Concevoir_un_algorithme_r\\u00e9cursif"
                        ],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "Conna\\u00eetre_les_3_questions_\\u00e0_se_poser"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "conna\\u00eetre_la_fonction_cons",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_primitives_de_construction_de_listes"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_la_repr\\u00e9sentation_d_une_liste"
                        ],
                        "hasLearning": [],
                        "hasTraining": [
                            "asker:8"
                        ]
                    }
                },
                {
                    "name": "conna\\u00eetre_les_primitives_d_acc\\u00e8s_aux_listes",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_fonctions_sur_les_listes"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [
                            "conna\\u00eetre_la_fonction_cdr",
                            "conna\\u00eetre_la_fonction_car"
                        ],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "conna\\u00eetre_les_fonctions_sur_les_listes",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_fonctions_pr\\u00e9d\\u00e9finies"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [
                            "conna\\u00eetre_les_primitives_de_construction_de_listes",
                            "conna\\u00eetre_les_primitives_de_test_sur_les_listes",
                            "conna\\u00eetre_la_repr\\u00e9sentation_d_une_liste",
                            "Savoir_choisir_les_bonnes_primitives_pour_construire_une_liste",
                            "conna\\u00eetre_les_primitives_d_acc\\u00e8s_aux_listes"
                        ],
                        "requires": [
                            "conna\\u00eetre_la_repr\\u00e9sentation_d_une_liste"
                        ],
                        "hasLearning": [],
                        "hasTraining": [
                            "asker:99",
                            "asker:114",
                            "asker:97",
                            "asker:20"
                        ]
                    }
                },
                {
                    "name": "conna\\u00eetre_la_fonction_cdr",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_primitives_d_acc\\u00e8s_aux_listes"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_la_repr\\u00e9sentation_d_une_liste"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "conna\\u00eetre_la_repr\\u00e9sentation_d_une_liste",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_fonctions_sur_les_listes"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "Conna\\u00eetre_la_d\\u00e9finition_d_une_liste_cha\\u00een\\u00e9e"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "conna\\u00eetre_les_fonctions_pr\\u00e9d\\u00e9finies",
                    "type": "Knowledge",
                    "mastery": 0.1111111111111111,
                    "trust": 0.1111111111111111,
                    "cover": 0.1111111111111111,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [
                            "Savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9pondant_\\u00e0_une_sp\\u00e9cification"
                        ],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [
                            "conna\\u00eetre_les_fonctions_sur_les_listes",
                            "conna\\u00eetre_les_fonctions_de_calcul",
                            "conna\\u00eetre_les_formes_sp\\u00e9ciales"
                        ],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "conna\\u00eetre_les_primitives_d_acc\\u00e8s_aux_arbres",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_primitives_sur_les_arbres"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_la_repr\\u00e9sentation_d_un_arbre_binaire"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "conna\\u00eetre_eval",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_formes_sp\\u00e9ciales"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Savoir_choisir_les_bonnes_primitives_pour_construire_une_liste",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_fonctions_sur_les_listes"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_les_primitives_de_construction_de_listes",
                            "conna\\u00eetre_la_repr\\u00e9sentation_d_une_liste",
                            "conna\\u00eetre_les_primitives_d_acc\\u00e8s_aux_listes"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "conna\\u00eetre_la_fonction_append",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_primitives_de_construction_de_listes"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "conna\\u00eetre_la_repr\\u00e9sentation_d_une_liste"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "conna\\u00eetre_les_fonctions_de_calcul",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_fonctions_pr\\u00e9d\\u00e9finies"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Conna\\u00eetre_la_d\\u00e9finition_d_une_liste_cha\\u00een\\u00e9e",
                    "type": "Knowledge",
                    "mastery": 0.4,
                    "trust": 1,
                    "cover": 1,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [
                            "Utiliser_les_TDA_liste_et_arbre"
                        ],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Conna\\u00eetre_la_d\\u00e9finition_d_un_ABR",
                    "type": "Knowledge",
                    "mastery": 0.6,
                    "trust": 1,
                    "cover": 1,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [
                            "Utiliser_les_TDA_liste_et_arbre"
                        ],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "Conna\\u00eetre_la_d\\u00e9finition_d_un_arbre_binaire"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Conna\\u00eetre_la_structure_d_une_fonction_r\\u00e9cursive",
                    "type": "Knowledge",
                    "mastery": 0.5,
                    "trust": 1,
                    "cover": 1,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [
                            "Concevoir_un_algorithme_r\\u00e9cursif"
                        ],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "conna\\u00eetre_les_formes_sp\\u00e9ciales",
                    "type": "Knowledge",
                    "mastery": 0.3333333333333333,
                    "trust": 0.3333333333333333,
                    "cover": 0.3333333333333333,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "conna\\u00eetre_les_fonctions_pr\\u00e9d\\u00e9finies"
                        ],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [
                            "conna\\u00eetre_eval",
                            "conna\\u00eetre_define",
                            "conna\\u00eetre_quote"
                        ],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Conna\\u00eetre_la_d\\u00e9finition_de_la_r\\u00e9cursivit\\u00e9",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [
                            "Comprendre_la_r\\u00e9cursivit\\u00e9"
                        ],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "conna\\u00eetre_les_primitives_sur_les_arbres",
                    "type": "Knowledge",
                    "mastery": 0.12000000000000002,
                    "trust": 0.30000000000000004,
                    "cover": 0.75,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [
                            "Savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9pondant_\\u00e0_une_sp\\u00e9cification"
                        ],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [
                            "conna\\u00eetre_les_primitives_de_test_sur_les_arbres",
                            "conna\\u00eetre_les_primitives_d_acc\\u00e8s_aux_arbres",
                            "conna\\u00eetre_la_repr\\u00e9sentation_d_un_arbre_binaire",
                            "conna\\u00eetre_les_primitives_de_construction_d_arbres"
                        ],
                        "requires": [
                            "conna\\u00eetre_la_repr\\u00e9sentation_d_un_arbre_binaire"
                        ],
                        "hasLearning": [],
                        "hasTraining": [
                            "asker:29",
                            "asker:33",
                            "asker:32",
                            "asker:31",
                            "asker:30"
                        ]
                    }
                },
                {
                    "name": "conna\\u00eetre_le_processus_d_\\u00e9valuation_de_l_appel_\\u00e0_une_fonction",
                    "type": "Knowledge",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [
                            "Savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9pondant_\\u00e0_une_sp\\u00e9cification"
                        ],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Comprendre_la_r\\u00e9cursivit\\u00e9",
                    "type": "Competency",
                    "mastery": 0,
                    "trust": 0,
                    "cover": 0,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [],
                        "hasSkill": [
                            "Reconna\\u00eetre_un_algorithme_r\\u00e9cursif_ou_non"
                        ],
                        "hasKnowledge": [
                            "Conna\\u00eetre_la_d\\u00e9finition_de_la_r\\u00e9cursivit\\u00e9"
                        ],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Savoir_\\u00e9crire_une_fonction_non_r\\u00e9cursive_en_Scheme",
                    "type": "Competency",
                    "mastery": 0.05555555555555555,
                    "trust": 0.1111111111111111,
                    "cover": 0.1111111111111111,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "Savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9pondant_\\u00e0_une_sp\\u00e9cification"
                        ],
                        "hasSkill": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_non_r\\u00e9cursive_sur_des_arbres",
                            "savoir_\\u00e9crire_une_fonction_Scheme_non_r\\u00e9cursive_sur_des_listes",
                            "savoir_\\u00e9crire_une_fonction_Scheme_non_r\\u00e9cursive_sur_des_nombres"
                        ],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Savoir_\\u00e9crire_une_fonction_r\\u00e9cursive_en_Scheme",
                    "type": "Competency",
                    "mastery": 0.32,
                    "trust": 0.39999999999999997,
                    "cover": 0.39999999999999997,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [
                            "Savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9pondant_\\u00e0_une_sp\\u00e9cification"
                        ],
                        "hasSkill": [
                            "savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9cursive_sur_des_nombres",
                            "savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9cursive_sur_des_listes",
                            "savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9cursive_sur_des_arbres"
                        ],
                        "hasKnowledge": [],
                        "isComposedOf": [],
                        "requires": [
                            "Savoir_\\u00e9crire_une_fonction_non_r\\u00e9cursive_en_Scheme",
                            "Concevoir_un_algorithme_r\\u00e9cursif"
                        ],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Savoir_\\u00e9crire_une_fonction_Scheme_r\\u00e9pondant_\\u00e0_une_sp\\u00e9cification",
                    "type": "Competency",
                    "mastery": 0.04666666666666667,
                    "trust": 0.07094017094017094,
                    "cover": 0.10555555555555556,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [],
                        "hasSkill": [
                            "\\u00e9valuer_une_expression_Scheme",
                            "Savoir_reconna\\u00eetre_si_une_fonction_r\\u00e9cursive_effectue_les_calculs_en_remontant_ou_en_descendant",
                            "Savoir_\\u00e9crire_une_fonction_r\\u00e9cursive_en_remontant_ou_descendant",
                            "savoir_reconna\\u00eetre_si_le_let_est_indispensable_ou_non",
                            "savoir_utiliser_let_pour_m\\u00e9moriser_une_valeur"
                        ],
                        "hasKnowledge": [
                            "conna\\u00eetre_les_structures_de_conditionnelle_alternative",
                            "conna\\u00eetre_les_fonctions_pr\\u00e9d\\u00e9finies",
                            "savoir_comment_d\\u00e9finir_l_ent\\u00eate_d_une_fonction",
                            "conna\\u00eetre_le_processus_d_\\u00e9valuation_de_l_appel_\\u00e0_une_fonction",
                            "conna\\u00eetre_les_primitives_sur_les_arbres",
                            "conna\\u00eetre_le_fonctionnement_du_let"
                        ],
                        "isComposedOf": [
                            "Savoir_\\u00e9crire_une_fonction_non_r\\u00e9cursive_en_Scheme",
                            "Savoir_\\u00e9crire_une_fonction_r\\u00e9cursive_en_Scheme"
                        ],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Utiliser_les_TDA_liste_et_arbre",
                    "type": "Competency",
                    "mastery": 0.30000000000000004,
                    "trust": 0.5,
                    "cover": 0.5,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [],
                        "hasSkill": [
                            "Savoir_parcourir_un_arbre_binaire",
                            "Savoir_parcourir_une_liste",
                            "Savoir_distinguer_un_ABR_d_un_arbre_non_ordonn\\u00e9"
                        ],
                        "hasKnowledge": [
                            "Conna\\u00eetre_la_d\\u00e9finition_d_une_liste_cha\\u00een\\u00e9e",
                            "Conna\\u00eetre_la_d\\u00e9finition_d_un_arbre_binaire",
                            "Conna\\u00eetre_la_d\\u00e9finition_d_un_ABR"
                        ],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                },
                {
                    "name": "Concevoir_un_algorithme_r\\u00e9cursif",
                    "type": "Competency",
                    "mastery": 0.6555555555555556,
                    "trust": 0.87037515143516,
                    "cover": 1,
                    "relations": {
                        "isSkillOf": [],
                        "isKnowledgeOf": [],
                        "composes": [],
                        "hasSkill": [],
                        "hasKnowledge": [
                            "Conna\\u00eetre_les_3_questions_\\u00e0_se_poser",
                            "Conna\\u00eetre_la_structure_d_une_fonction_r\\u00e9cursive",
                            "Savoir_dans_quel_ordre_se_poser_les_3_questions"
                        ],
                        "isComposedOf": [],
                        "requires": [],
                        "hasLearning": [],
                        "hasTraining": []
                    }
                }
            ],
            "resources": [
                {
                    "id": "asker:26",
                    "name": "Parcours Infixe",
                    "interactivityType": "active",
                    "learningResourceType": "matching",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/26",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:28",
                    "name": "Parcours Postfixe",
                    "interactivityType": "active",
                    "learningResourceType": "matching",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/28",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:27",
                    "name": "Parcours Prefixe",
                    "interactivityType": "active",
                    "learningResourceType": "matching",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/27",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:102",
                    "name": "Utilit\\u00e9 du let",
                    "interactivityType": "active",
                    "learningResourceType": "matching",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/102",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:19",
                    "name": "Que fait la fonction mystere ?",
                    "interactivityType": "active",
                    "learningResourceType": "matching",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/19",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:172",
                    "name": "Que nous renvoie la fonction mystere ?",
                    "interactivityType": "active",
                    "learningResourceType": "matching",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/172",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:103",
                    "name": "Fonctions en montant ou en descendant",
                    "interactivityType": "active",
                    "learningResourceType": "matching",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/103",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:286",
                    "name": "Unification sur les listes",
                    "interactivityType": "active",
                    "learningResourceType": "choice",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/286",
                    "author": "nathalie.guin",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:35",
                    "name": "Arbre binaire de recherche",
                    "interactivityType": "active",
                    "learningResourceType": "matching",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/35",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:417",
                    "name": "D\\u00e9finition du Let",
                    "interactivityType": "active",
                    "learningResourceType": "choice",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/417",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:2",
                    "name": "Evaluation d'expressions",
                    "interactivityType": "active",
                    "learningResourceType": "choice",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/2",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:4",
                    "name": "Definition en Scheme d'arbre contenant des symboles",
                    "interactivityType": "active",
                    "learningResourceType": "matching",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 3,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/4",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:434",
                    "name": "Definition en Scheme d'arbre contenant des nombres",
                    "interactivityType": "active",
                    "learningResourceType": "matching",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 3,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/434",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:8",
                    "name": "Evaluation d'expressions avec la fonction cons",
                    "interactivityType": "active",
                    "learningResourceType": "choice",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/8",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:99",
                    "name": "Primitives sur les listes : type d'entr\\u00e9e",
                    "interactivityType": "active",
                    "learningResourceType": "matching",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/99",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:114",
                    "name": "Construction de listes",
                    "interactivityType": "active",
                    "learningResourceType": "matching",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/114",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:97",
                    "name": "Primitives sur les listes : type de sortie",
                    "interactivityType": "active",
                    "learningResourceType": "matching",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/97",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:20",
                    "name": "Primitives sur les listes : description",
                    "interactivityType": "active",
                    "learningResourceType": "matching",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/20",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:29",
                    "name": "Primitives sur les arbres : leurs descriptions",
                    "interactivityType": "active",
                    "learningResourceType": "matching",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/29",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:33",
                    "name": "Primitives sur les arbres : test, acc\\u00e8s ou construction ?",
                    "interactivityType": "active",
                    "learningResourceType": "matching",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/33",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:32",
                    "name": "Primitives sur les arbres : les types d'entr\\u00e9e",
                    "interactivityType": "active",
                    "learningResourceType": "matching",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/32",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:31",
                    "name": "Primitives sur les arbres : les types de sortie",
                    "interactivityType": "active",
                    "learningResourceType": "matching",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/31",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                },
                {
                    "id": "asker:30",
                    "name": "Primitives sur les arbres : leurs noms",
                    "interactivityType": "active",
                    "learningResourceType": "matching",
                    "significanceLevel": 1,
                    "difficulty": 2,
                    "typicalLearningTime": 5,
                    "learningPlatform": "asker",
                    "location": "https:\\/\\/asker.univ-lyon1.fr\\/front\\/#\\/learner\\/model\\/30",
                    "author": "m.lefevre",
                    "language": "fr",
                    "generative": true
                }
            ]
        }
    }

    /**
     * @returns {{objects : Array<FrameworkNode>}}, a test framework from real application, with score on each node (y, cover and trust).
     */
    public static getScoredFrameworkSample(): object {

        let framework = Utils.getFrameworkSample();

        let leaves = new Set;

        let setRandomValues = function (node: FrameworkNode, leaf: boolean): void {
            if (leaf) {
                if (Math.random() > 0.75) {
                    node.mastery = 0;
                    node.trust = 0;
                    node.resourcesCompleted = 0;
                } else {
                    node.mastery = Math.floor(Math.random() * 11) / 10;
                    node.trust = Math.floor((Math.random() * 4) + 1) / 10;
                    node.resourcesCompleted = node.trust;
                }
                node.cover = undefined;
            } else {
                node.trust = Math.floor(Math.random() * 4) / 10;
                node.mastery = Math.floor(Math.random() * 11) / 10;
                node.cover = 0;
                node.resourcesCompleted = undefined;
            }
        }

        let findNode = function (fw: Array<FrameworkNode>, name: string): FrameworkNode {
            for (let i = 0; i < fw.length; i++) {
                if (fw[i].name === name) return fw[i];
            }
            return null;
        }

        framework['objects'].forEach((node: FrameworkNode) => {
            node.children = [node.relations.hasSkill, node.relations.hasKnowledge, node.relations.isComposedOf].flat();
            node.parents = [node.relations.isSkillOf, node.relations.isKnowledgeOf, node.relations.composes].flat();
        });

        framework['objects'].forEach((node: FrameworkNode) => {
            if (node.children.length === 0) {
                // Save the leaves for propagation later
                node.parents.forEach(child => {
                    leaves.add(child);
                });
                setRandomValues(node, true);
            } else {
                (Math.random() > 0.75) ? setRandomValues(node, false) : node.cover = node.mastery = node.trust = 0;
            }
        });

        while (leaves.size > 0) {
            let nextLeaves = new Set();
            leaves.forEach((leafName: string) => {
                let leaf = findNode(framework['objects'], leafName);
                let children: Array<FrameworkNode> = [];
                leaf.children.forEach(childName => {
                    children.push(findNode(framework['objects'], childName));
                });
                let meanCover = 0;
                let meanMaster = 0;
                let meanTrust = 0;
                children.forEach(child => {
                    if (child.resourcesCompleted !== undefined) {
                        meanCover += (child.resourcesCompleted !== 0) ? 1 : 0;
                    } else {
                        meanCover += child.cover;
                    }
                    meanMaster += child.mastery;
                })

                meanCover /= Math.max(children.length, 1);
                meanMaster /= Math.max(children.length, 1);
                meanTrust = (leaf.trust + meanCover) / 2;

                leaf.cover = meanCover;
                leaf.masteyr = Math.round(meanMaster);
                leaf.trust = Math.round(meanTrust);

                leaf.parents.forEach(child => {
                    nextLeaves.add(child);
                });

            });
            leaves = nextLeaves;
        }
        return framework;
    }

    public static makeId(prefix: string = '', length: number = 6): string {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return prefix + Date.now() + result;
    }
}

export {Utils}
