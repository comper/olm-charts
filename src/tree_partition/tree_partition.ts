import {FrameworkTree, FrameworkTreeNode} from "../core/framework";
import {Utils} from "../core/utils";
import * as d3 from 'd3';

interface TreePartitionHierarchy {
    name: string,
    color: string,
    hash: boolean,
    children?: Array<TreePartitionHierarchy>,
    src: FrameworkTreeNode,
    value?: number,
}

interface TreePartitionD3Data extends d3.HierarchyNode<TreePartitionHierarchy> {
    x0: number,
    y0: number,
    x1: number,
    y1: number,
    data: TreePartitionHierarchy
}

interface TreePartitionConfig {
    useLegend?: boolean,
    useHash?: boolean,
    hashTreshold?: number,
    colors?: Array<TreePartitionColor>,
    noValueColor?: string,
    formatMastery?: string,
    formatCover?: string,
    formatTrust?: string,
    fontColor?: string,
    backgroundColor?: string
}

interface TreePartitionColor {
    to?: number,
    color: string
}

interface TreePartitionI18NLanguages {
    fr: TreePartitionI18N,

    [key: string]: TreePartitionI18N
}

interface TreePartitionI18N {
    information: {
        mastery: {
            name: string,
            description: string
        },
        trust: {
            name: string,
            description: string
        },
        cover: {
            name: string,
            description: string
        }
    },
    legend: {
        noMastery: string,
        masteryBetween: string,
        trustTreshold: string
    }
}

export class TreePartition {
    public onMouseOver: (node: FrameworkTreeNode) => void = () => {
    };
    public onMouseOut: (node: FrameworkTreeNode) => void = () => {
    };

    // Color settings
    private colors: Array<TreePartitionColor> = [{to: .25, color: "#cf000f"}, {to: .5, color: "#f57f17"}, {
        to: .75,
        color: "#ffee58"
    }, {color: "#4caf50"}];
    private noValueColor: string = '#808080';
    private fontColor: string = 'rgba(255, 255, 255, .85)';
    private backgroundColor: string = '#343a40';

    // Hash settings
    private useHash: boolean = true;
    private hashTreshold: number = 0;

    // Legend settings
    private useLegend: boolean = true;

    // Infos displayed
    private formatMastery: string = 'percentage';    // '2decimal', 'percentage'
    private formatCover: string = 'percentage';  //
    private formatTrust: string = 'percentage';  //

    // Data
    private framework: FrameworkTree = null;
    private hierarchy: TreePartitionHierarchy = null;

    // HTMLElements
    private elem: HTMLElement = null;
    private svg: SVGElement = null;
    private svgG: SVGElement = null;
    private svgId: string = null;
    private infos: HTMLElement = null;

    // D3 consts
    private viewBoxWidth: number = 800;
    private viewBoxHeight: number = 400;

    // I18N
    private language: string = 'fr';
    readonly i18n: TreePartitionI18NLanguages = {
        'fr': {
            'information': {
                'mastery': {
                    'name': 'Taux de maîtrise',
                    'description': 'Estime ton niveau de maîtrise pour chaque notion, calculé en fonction des travaux effectués en lien avec cette notion.'
                },
                'trust': {
                    'name': 'Taux de confiance',
                    'description': 'Indique la confiance dans le calcul du taux de maîtrise. Cet indice dépend du nombre et de la nature des travaux effectués.'
                },
                'cover': {
                    'name': 'Taux de couverture',
                    'description': 'Indique le pourcentage des sous-notions travaillées.'
                }
            },
            'legend': {
                'noMastery': 'Maîtrise non évaluée',
                'masteryBetween': 'maîtrise ∈ ',
                'trustTreshold': 'confiance < '
            }
        }
    };

    constructor(elem: HTMLElement, framework: FrameworkTree, config: TreePartitionConfig = {}) {
        this.elem = elem;
        this.framework = FrameworkTree.copy(framework);

        this.elem.style.position = 'relative';
        this.viewBoxWidth = elem.offsetWidth;
        this.viewBoxHeight = elem.offsetHeight;

        if (config.useHash !== undefined) this.useHash = config.useHash;
        if (config.hashTreshold !== undefined) this.hashTreshold = config.hashTreshold;
        if (config.useLegend !== undefined) this.useLegend = config.useLegend;
        if (config.colors !== undefined) this.colors = config.colors;
        if (config.noValueColor !== undefined) this.noValueColor = config.noValueColor;
        if (config.formatMastery !== undefined) this.formatMastery = config.formatMastery;
        if (config.formatCover !== undefined) this.formatCover = config.formatCover;
        if (config.formatTrust !== undefined) this.formatTrust = config.formatTrust;
        if (config.fontColor !== undefined) this.fontColor = config.fontColor;
        if (config.backgroundColor !== undefined) this.backgroundColor = config.backgroundColor;
        this.hierarchy = this._buildHierarchy();

        let redraw = () => {
            this.elem.innerHTML = "";
            this.viewBoxWidth = elem.offsetWidth;
            this.viewBoxHeight = elem.offsetHeight;
            this.draw();
        };
        redraw.bind(this);
        window.addEventListener("resize", redraw);
    }

    public draw(svgId: string = Utils.makeId()) {
        // Creates the svg element. The viewBox dmakes the svg "responsive".
        this.svgId = svgId;
        this.svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        this.svg.id = this.svgId;

        // Set the boundaries of the element. Ignores the legend offset if we do not display it.
        let width: string = this.viewBoxWidth.toString();
        let height: string = this.viewBoxHeight.toString();
        this.svg.setAttribute('viewBox', '0 0 ' + width + ' ' + height);
        this.svg.setAttribute('width', '100%');
        this.svg.setAttribute('height', '100%');
        this.svg.style.cssText += 'width: 100% !important; height: 100% !important;';
        this.svgG = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        this.svg.appendChild(this.svgG);
        this.elem.appendChild(this.svg);

        this._drawInfos();

        if (this.useHash) this._drawHash();
        this._drawPacks();
        if (this.useLegend) this._drawLegend();
    }

    private _drawInfos(): void {
        this.infos = document.createElement('div');
        this.infos.innerHTML = `<svg width="20px" height="20px" viewBox="0 0 16 16"  fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                    <path d="M5.255 5.786a.237.237 0 0 0 .241.247h.825c.138 0 .248-.113.266-.25.09-.656.54-1.134 1.342-1.134.686 0 1.314.343 1.314 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.003.217a.25.25 0 0 0 .25.246h.811a.25.25 0 0 0 .25-.25v-.105c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.267 0-2.655.59-2.75 2.286zm1.557 5.763c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z"/>
                                </svg>`;
        this.infos.setAttribute('style', 'position: absolute; top: 0; left : 0; cursor: help; color:' + this.fontColor);

        let infoPanel = document.createElement('div');
        infoPanel.innerHTML = `<h4> Informations </h4>
                                <hr style="background-color:${this.fontColor} "/>
                                <p><b>${this.i18n[this.language].information.mastery.name}</b>:<br/>${this.i18n[this.language].information.mastery.description}</p>
                                <p><b>${this.i18n[this.language].information.trust.name}</b>:<br/>${this.i18n[this.language].information.trust.description}</p>
                                <p><b>${this.i18n[this.language].information.cover.name}</b>:<br/>${this.i18n[this.language].information.cover.description}</p>`;

        let infoPanelStyle: string = `border-radius:    15px; 
                                        opacity:          .95; 
                                        position:         absolute; 
                                        padding:          30px; 
                                        top:              0; 
                                        left:             0; 
                                        color:            ${this.fontColor};
                                        background-color: ${this.backgroundColor}; 
                                        width:            ${this.viewBoxWidth.toString()}px; 
                                        height:           ${this.viewBoxHeight.toString()}px; 
                                        display:          none`;

        infoPanel.setAttribute('style', infoPanelStyle);
        this.elem.appendChild(infoPanel);
        this.elem.appendChild(this.infos);
        this.infos.addEventListener('mouseenter', () => {
            infoPanel.style.display = 'block';
        });
        this.infos.addEventListener('mouseleave', () => {
            infoPanel.style.display = 'none';
        });
    }

    private _drawHash(): void {
        let s = 4;

        d3.select(this.svg)
            .append("defs")
            .append('pattern')
            .attr('id', 'diagonalHatch')
            .attr('patternUnits', 'userSpaceOnUse')
            .attr('width', s)
            .attr('height', s)
            .append('circle')
            .attr('cx', s / 2)
            .attr('cy', s / 2)
            .attr('r', 1)
            .attr('stroke-width', 0)
            .attr('fill', '#404040');

        // Diagonal Hash
        // .append('path')
        // .attr('d', `M 0,${s} l ${s},${-s} M ${-s / 4},${s / 4} l ${s / 2},${-s / 2} M ${3 / 4 * s},${5 / 4 * s} l ${s / 2},${-s / 2}`)
        // .attr('stroke-width', 2);
    }

    private _drawPacks(): void {
        var partitionLayout = d3.partition();
        let offsetHeight: number = (5 * 26) + 10; // THe legend height + 10 padding
        let height: number = this.viewBoxHeight - offsetHeight;
        let width: number = this.viewBoxWidth;
        partitionLayout.size([width, height]);
        // partitionLayout.padding(2);

        let root = d3.hierarchy(this.hierarchy);
        root.sum(d => d.value);
        partitionLayout(root);

        var partitionNodes = d3.select(this.svgG)
            .selectAll("g")
            .data(root.descendants())
            .enter()
            .append('g').attr('class', 'node')
            .attr('transform', (d: TreePartitionD3Data) => 'translate(' + [d.x0, d.y0 + offsetHeight] + ')')
            .on('mouseover', this._handleMouseOver.bind(this))
            .on('mouseout', this._handleMouseOut.bind(this));

        partitionNodes
            .append('rect')
            .classed('the-node', true)
            .attr('id', d => 'olm-partition-' + d.data.src.id)
            .attr('width', (d: TreePartitionD3Data) => d.x1 - d.x0)
            .attr('height', (d: TreePartitionD3Data) => d.y1 - d.y0)
            .style('fill', (d: TreePartitionD3Data) => d.data.color)
            .style('cursor', 'pointer')
            .style('stroke', '#2f2f2f')

        partitionNodes
            .filter(d => d.data.hash)
            .append('rect')
            .classed('the-node', true)
            .attr('width', (d: TreePartitionD3Data) => d.x1 - d.x0)
            .attr('height', (d: TreePartitionD3Data) => d.y1 - d.y0)
            .attr('fill', 'url(#diagonalHatch)')
            .style('cursor', 'pointer')
            .style('stroke', '#2f2f2f')
    }

    private _drawLegend(): void {
        let packNodes = d3.select(this.svg)
        let size: number = 16;
        let x: number = 10 + 20; // 20 is help icon button width;
        let y: number = 0;

        let _generic_draw = (color: string, text: string) => {
            packNodes.append("rect")
                .attr("x", x)
                .attr("y", y) // 100 is where the first dot appears. 25 is the distance between dots
                .attr("width", size)
                .attr("height", size)
                .style("fill", color);

            packNodes.append("text")
                .attr("x", x + size + 10)
                .attr("y", y + size) // 100 is where the first dot appears. 25 is the distance between dots
                .style("fill", this.fontColor)
                .text(text)
                .attr("text-anchor", "left")
                .style("alignment-baseline", "baseline")
        }

        // No mastery score
        _generic_draw(this.noValueColor, this.i18n[this.language].legend.noMastery);
        y += size + 10;

        for (let i = 0; i < this.colors.length; i++) {
            let text: string = this.i18n[this.language].legend.masteryBetween;
            if (i === 0) text += `[${this._formatValue('mastery', 0)}`;
            else text += `]${this._formatValue('mastery', this.colors[i - 1].to)}`;
            if (i === this.colors.length - 1) text += `,${this._formatValue('mastery', 1)}]`;
            else text += `,${this._formatValue('mastery', this.colors[i].to)}]`;
            _generic_draw(this.colors[i].color, text);
            y += size + 10;
        }

        // Trust score legend
        if (this.useHash) {
            _generic_draw('rgba(255, 255, 2555, .8)', this.i18n[this.language].legend.trustTreshold + this._formatValue('trust', this.hashTreshold));
            packNodes.append("rect")
                .attr("x", x)
                .attr("y", y) // 100 is where the first dot appears. 25 is the distance between dots
                .attr("width", size)
                .attr("height", size)
                .style("fill", 'url(#diagonalHatch)');
        }
    }

    private _drawDetails(d: TreePartitionD3Data): void {
        let y = 16;
        let x = 220;
        let packNodes = d3.select(this.svg);
        packNodes.append("text")
            .attr("x", x)
            .attr("y", y)
            .style("fill", this.fontColor)
            .text(d.data.src.data.name)
            .attr('class', 'olm-treepack-label')
            .attr("text-anchor", "left")
            .style("alignment-baseline", "baseline")
            .style("font-weight", 'bold');

        y += 16 + 10;

        packNodes.append("text")
            .attr("x", x)
            .attr("y", y)
            .style("fill", this.fontColor)
            .text(`Taux de maîtrise : ${this._formatValue('mastery', d.data.src.data.mastery)}`)
            .attr('class', 'olm-treepack-label')
            .attr("text-anchor", "left")
            .style("alignment-baseline", "baseline");

        y += 16 + 10;

        packNodes.append("text")
            .attr("x", x)
            .attr("y", y)
            .style("fill", this.fontColor)
            .text("Taux de confiance : " + this._formatValue('trust', d.data.src.data.trust))
            .attr('class', 'olm-treepack-label')
            .attr("text-anchor", "left")
            .style("alignment-baseline", "baseline");

        y += 16 + 10;

        packNodes.append("text")
            .attr("x", x)
            .attr("y", y)
            .style("fill", this.fontColor)
            .text("Taux de couverture : " + this._formatValue('cover', d.data.src.data.cover))
            .attr('class', 'olm-treepack-label')
            .attr("text-anchor", "left")
            .style("alignment-baseline", "baseline");
    }

    public getSVGId(): string {
        return this.svgId;
    }

    /**
     * Builds hierarchy
     * @returns hierarchy
     */
    private _buildHierarchy(): TreePartitionHierarchy {
        function recurse(currentNode: FrameworkTreeNode) {
            let data_node: TreePartitionHierarchy = null;
            // Sets the colors. If we got a leaf, put grey if trust > 0
            let color = this.noValueColor;
            if (currentNode.data.mastery === undefined) color = this.noValueColor;
            else if ((currentNode.children.length !== 0 && currentNode.data.cover > 0) || currentNode.data.trust > 0) {
                let i = 0;
                color = this.colors[i].color;
                while (i < this.colors.length - 1 && currentNode.data.mastery > this.colors[i].to) {
                    i++;
                    color = this.colors[i].color;
                }
            }
            // Creates the tree pack hirarchy objects.
            // With children if not a leaf, with value = 1 otherwise.
            if (currentNode.children.length > 0) {
                data_node = {
                    'name': currentNode.data.name,
                    'children': [],
                    'color': color,
                    'hash': (this.useHash && currentNode.data.trust < this.hashTreshold),
                    'src': currentNode
                }
            } else {
                data_node = {
                    'name': currentNode.data.name,
                    'color': color,
                    'value': 1,
                    'hash': (this.useHash && currentNode.data.trust < this.hashTreshold),
                    'src': currentNode
                }
            }
            // Creates the children of the current node if they exist.
            for (var i = 0, length = currentNode.children.length; i < length; i++) {
                let nextNode = this.framework.getNode(currentNode.children[i]);
                data_node.children.push(recurse.bind(this, nextNode)());
            }
            return data_node;
        }

        return recurse.bind(this, this.framework.getRoot())();
    }

    private _handleMouseOver(e: any, d: TreePartitionD3Data): void {
        let id = 'olm-partition-' + d.data.src.id;
        d3.select(document.getElementById(id))
            .transition().duration(200)
            .style('fill', "rgba(211,211,211,0.8)")

        this._drawDetails(d);

        this.onMouseOver(d.data.src);
    }

    private _handleMouseOut(e: any, d: TreePartitionD3Data): void {
        let id = 'olm-partition-' + d.data.src.id;
        d3.select(document.getElementById(id))
            .transition().duration(200)
            .style('fill', d.data.color);

        d3.selectAll(".olm-treepack-label").remove();

        this.onMouseOut(d.data.src);
    }

    // Utilities ---------------------------------------------------------------
    private _formatValue(type: string, value: number): string {
        if (isNaN(value) || value === undefined || value === null) return '-';
        switch (type) {
            case "mastery":
                switch (this.formatMastery) {
                    case '1decimal':
                        return (Math.round(value * 10) / 10).toString();
                    case '2decimal':
                        return (Math.round(value * 100) / 100).toString();
                    case 'percentage':
                        return Math.round(value * 100).toString() + '%';
                }
                break;
            case "cover":
                switch (this.formatCover) {
                    case 'percentage':
                        return Math.round(value * 100).toString() + '%';
                }
                break;
            case "trust":
                switch (this.formatTrust) {
                    case 'percentage':
                        return Math.round(value * 100).toString() + '%';
                }
                break;
        }
        return 'a';
    }
}
