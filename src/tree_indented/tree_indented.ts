import {FrameworkTree, FrameworkTreeNode} from "../core/framework";

interface TreeIndentedNode {
    id: string,
    parent: string,
    src: FrameworkTreeNode
}

interface TreeIndentedConfig {
    colors?: Array<TreeIndentedColor>,
    noValueColor?: string,
    fontColor?: string,
    fontHoverColor?: string,
    showMastery?: boolean,
    showTrust?: boolean,
    showCover?: boolean,
    showExercises?: boolean,
    coverAsPie?: boolean,
    showColorsInfo?: boolean,
    formatMastery?: string,
    formatCover?: string,
    formatTrust?: string,
    backgroundColor?: string
}

interface TreeIndentedColor {
    to?: number,
    color: string
}

interface TreeIndentedI18NLanguages {
    fr: TreeIndentedI18N,

    [key: string]: TreeIndentedI18N
}

interface TreeIndentedI18N {
    headers: {
        mastery: string,
        cover: string,
        trust: string
    },
    information: {
        mastery: {
            name: string,
            description: string
        },
        trust: {
            name: string,
            description: string
        },
        cover: {
            name: string,
            description: string
        }
    }
}

export class TreeIndented {
    // Event functions
    public onClick: (node: FrameworkTreeNode) => void = null;
    public onVisuCLick: (node: FrameworkTreeNode) => void = null;
    public onExerciseClick: (exercise: HTMLElement) => void = null;
    public onColumnClick: (column: HTMLElement, type: string) => void = null;
    public onElemClick: (node: FrameworkTreeNode, type: string) => void = null;
    public onMouseEnter: (node: FrameworkTreeNode) => void = null;
    public onMouseLeave: (node: FrameworkTreeNode) => void = null;
    public onMouseOver: (node: FrameworkTreeNode) => void = null;
    public onDbClick: (node: FrameworkTreeNode) => void = null;


    // Colors settings
    private colors: Array<TreeIndentedColor> = [{to: .25, color: "#cf000f"}, {to: .5, color: "#f57f17"}, {
        to: .75,
        color: "#ffee58"
    }, {color: "#4caf50"}];
    private noValueColor: string = '#808080';
    private fontColor: string = 'rgba(255, 255, 255, .85)';
    private fontHoverColor: string = 'rgba(255, 255, 255, 1)';
    private backgroundColor: string = '#343a40';

    // Infos displayed
    private showMastery: boolean = true;
    private formatMastery: string = 'percentage';    // '2decimal', 'percentage'
    private showCover: boolean = true;
    private formatCover: string = 'percentage';  //
    private showTrust: boolean = true;
    private formatTrust: string = 'percentage';  //
    private showExercises: boolean = true;
    private coverAsPie: boolean = true;

    private elem: HTMLElement = null;
    private htmlRoot: HTMLElement = null;
    private framework: FrameworkTree = null;
    private frameworkTree: Array<TreeIndentedNode> = null;
    private infos: HTMLElement = null;
    private slider: ChildNode = null;

    // I18N
    readonly language: string = 'fr';
    readonly i18n: TreeIndentedI18NLanguages = {
        'fr': {
            'headers': {
                'cover': 'couverture',
                'mastery': 'maîtrise',
                'trust': 'confiance',
            },
            'information': {
                'mastery': {
                    'name': 'Taux de maîtrise',
                    'description': 'Estime ton niveau de maîtrise pour chaque notion, calculé en fonction des travaux effectués en lien avec cette notion, représenté par la couleur de la pastille'
                },
                'trust': {
                    'name': 'Taux de confiance',
                    'description': 'Indique la confiance dans le calcul du taux de maîtrise. Cet indice dépend du nombre et de la nature des travaux effectués.'
                },
                'cover': {
                    'name': 'Taux de couverture',
                    'description': 'Indique le pourcentage des sous-notions travaillées, représenté par le remplissage de la pastille.'
                }
            }
        }
    };

    // Caret objet utilities
    readonly _caretDown: string = `<svg width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M3.204 5L8 10.481 12.796 5H3.204zm-.753.659l4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/>
                                    </svg>`;
    readonly _sCaretDown: string = `display: inline-block; 
                                     margin-left: 10px;
                                     vertical-align: bottom;
                                     padding-left: 3px;
                                     padding-bottom: 3px;
                                     box-sizing: border-box;
                                     width: 21px;
                                     cursor: pointer`;
    readonly _caretRight: string = `<svg width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M6 12.796L11.481 8 6 3.204v9.592zm.659.753l5.48-4.796a1 1 0 0 0 0-1.506L6.66 2.451C6.011 1.885 5 2.345 5 3.204v9.592a1 1 0 0 0 1.659.753z"/>
                                    </svg>`;
    readonly _sCaretRight: string = `display: inline-block; 
                                     margin-left: 10px;
                                     vertical-align: bottom;
                                     padding-left: 5px;
                                     padding-bottom: 3px;
                                     box-sizing: border-box;
                                     width: 21px;
                                     cursor: pointer`;
    readonly _sNoCaret: string = `display: inline-block; 
                                     margin-left: 10px;
                                     width : 21px;
                                     vertical-align: bottom;
                                     padding-left: 5px;
                                     padding-bottom: 3px;
                                     box-sizing: border-box;
                                     pointer-event: none;`;

    readonly _sCarretExercise: string = `display: inline-block; 
                                     margin-left: 10px;
                                     width : 40px;
                                     float : left;
                                     vertical-align: bottom;
                                     padding-left: 5px;
                                     padding-bottom: 3px;
                                     box-sizing: border-box;
                                     pointer-event: none;
                                     height: 1.5em;
                                     margin-left: 20px;`;

    readonly _sCarretExerciseRoot: string = `display: inline-block; 
                                     margin-left: 10px;
                                     float : left;
                                     vertical-align: bottom;
                                     width: 20px;
                                     padding-left: 5px;
                                     padding-bottom: 3px;
                                     box-sizing: border-box;
                                     pointer-event: none;
                                     height: 1.5em;
                                     margin-left: 20px;`;

    constructor(elem: HTMLElement, framework: FrameworkTree, config: TreeIndentedConfig) {
        this.elem = elem;
        this.elem.style.position = 'relative';

        this.framework = FrameworkTree.copy(framework);
        this.frameworkTree = this._buildHierarchy();

        if (config.fontColor !== undefined) this.fontColor = config.fontColor;
        if (config.fontHoverColor !== undefined) this.fontHoverColor = config.fontHoverColor;
        if (config.colors !== undefined) this.colors = config.colors;
        if (config.noValueColor !== undefined) this.noValueColor = config.noValueColor;
        if (config.showMastery !== undefined) this.showMastery = config.showMastery;
        if (config.showTrust !== undefined) this.showTrust = config.showTrust;
        if (config.showCover !== undefined) this.showCover = config.showCover;
        if (config.showExercises !== undefined) this.showExercises = config.showExercises;
        if (config.coverAsPie !== undefined) this.coverAsPie = config.coverAsPie;
        if (config.formatMastery !== undefined) this.formatMastery = config.formatMastery;
        if (config.formatCover !== undefined) this.formatCover = config.formatCover;
        if (config.formatTrust !== undefined) this.formatTrust = config.formatTrust;
        if (config.backgroundColor !== undefined) this.backgroundColor = config.backgroundColor;

        this._createHTMLRoot();
        let redraw = () => {
            this.elem.innerHTML = "";
            this._createHTMLRoot();
            this.draw();
        };
        redraw.bind(this);
        window.addEventListener("resize", redraw);
    }

    private rgba2hex(orig: string) {
        let a, isPercent,
            rgb = orig.replace(/\s/g, '').match(/^rgba?\((\d+),(\d+),(\d+),?([^,\s)]+)?/i),
            alpha = (rgb && rgb[4] || "").trim(),
            hex = rgb ?
                // @ts-ignore
                (rgb[1] | 1 << 8).toString(16).slice(1) +
                // @ts-ignore
                (rgb[2] | 1 << 8).toString(16).slice(1) +
                // @ts-ignore
                (rgb[3] | 1 << 8).toString(16).slice(1) : orig;

        if (alpha !== "") {
            a = alpha;
        } else {
            a = 0o1;
        }
        // multiply before convert to HEX
        // @ts-ignore
        a = ((a * 255) | 1 << 8).toString(16).slice(1)
        hex = hex + a;

        return hex;
    }

    public draw(): void {
        // If we have at least one stat to display, we create the header
        if (this.showCover || this.showMastery || this.showTrust) this._drawHeader();

        for (let i = 0; i < this.frameworkTree.length; i++) {
            let node = this.frameworkTree[i];
            let elem = document.createElement('div');
            elem.id = "node:" + node.src.data.name;
            elem.style.color = this.fontColor;
            elem.style.fontFamily = '"Times New Roman", Times, serif';
            let contentNode = document.createElement('div');
            let caret = this._createCaret(node);
            contentNode.appendChild(caret);

            contentNode.appendChild(this._createScoreElement(node));

            let content = this._createContentElement(node);
            if (this.onClick !== null) content.style.cursor = 'pointer';
            contentNode.appendChild(content);

            let contentPacking: Array<HTMLElement> = [content];
            let showable = (node.src.data.cover > 0);
            if (this.showCover) {
                let coverValue: string = this._formatValue('cover', node.src.data.cover, showable);
                let cover: HTMLElement = this._createStatElement(coverValue);
                contentNode.appendChild(cover);
                contentPacking.push(cover);
                if (this.onElemClick !== null) cover.style.cursor = 'pointer';
                if (this.onElemClick !== null) cover.addEventListener('click', this._handleContentElemClick.bind(this, node, 'cover'));
            }
            if (this.showTrust) {
                let trustValue: string = this._formatValue('trust', node.src.data.trust, showable);
                let trust: HTMLElement = this._createStatElement(trustValue);
                contentNode.appendChild(trust);
                contentPacking.push(trust);
                if (this.onElemClick !== null) trust.style.cursor = 'pointer';
                if (this.onElemClick !== null) trust.addEventListener('click', this._handleContentElemClick.bind(this, node, 'trust'));
            }
            if (this.showMastery) {// && node.src.data.trust > 0){
                let masteryValue: string = this._formatValue('mastery', node.src.data.mastery, showable);
                let mastery: HTMLElement = this._createStatElement(masteryValue);
                contentNode.appendChild(mastery);
                contentPacking.push(mastery);
                if (this.onElemClick !== null) mastery.style.cursor = 'pointer';
                if (this.onElemClick !== null) mastery.addEventListener('click', this._handleContentElemClick.bind(this, node, 'mastery'));
            }

            elem.appendChild(contentNode);
            // Gestion of events
            content.addEventListener('mouseenter', this._handleContentMouseEnter.bind(this, contentPacking, node));
            content.addEventListener('mouseleave', this._handleContentMouseLeave.bind(this, contentPacking, node));
            content.addEventListener('mouseover', this._handleContentMouseOver.bind(this, node));
            content.addEventListener('dblclick', this._handleContentDbClick.bind(this, node));

            if (this.onClick !== null) content.addEventListener('click', this._handleContentClick.bind(this, node));

            let children = this._createChildrenContainerElement(node.id);
            elem.appendChild(children);
            if (node.parent !== undefined && node.parent !== null) {
                document.getElementById('olm-tree-indented-bloc-' + node.parent).appendChild(elem);
            } else {
                this.htmlRoot.appendChild(elem);
            }


            if (this.showExercises) {
                for (let resource of node.src.resources) {
                    let exerciseContainer: HTMLElement = document.createElement('span');
                    exerciseContainer.style.display = "flex";
                    let caretExercise = document.createElement('span');


                    if (node.src.children.length > 0) {
                        caretExercise.setAttribute('style', this._sCarretExerciseRoot);
                        if (this.rgba2hex(this.backgroundColor) >= "7d7d7d") {
                            exerciseContainer.style.borderLeft = "dashed 1px #808080";
                        } else {
                            exerciseContainer.style.borderLeft = "dashed 1px rgba(255, 255, 255, .3)";
                        }
                        exerciseContainer.style.marginLeft = "20px";
                    } else {
                        caretExercise.setAttribute('style', this._sCarretExercise);
                        if (this.rgba2hex(this.backgroundColor) >= "7d7d7d") {
                            caretExercise.style.borderLeft = "dashed 1px #808080";
                        } else {
                            caretExercise.style.borderLeft = "dashed 1px rgba(255, 255, 255, .3)";
                        }
                    }
                    exerciseContainer.appendChild(caretExercise);

                    let exercise: HTMLElement = document.createElement('div');
                    let exerciseOrCourse = resource.interactivityType == "active" ? "Exercice" : "Cours";
                    exercise.innerHTML = `<div> <b>${exerciseOrCourse} :</b> ${resource.name}</div>`
                    exercise.id = "exercise-" + resource.name;
                    exercise.className = "exercise-olm";
                    exerciseContainer.appendChild(exercise);
                    if (node.parent !== undefined && node.parent !== null) {
                        if (node.src.children.length > 0) {
                            document.getElementById("node:" + node.src.data.name).insertBefore(exerciseContainer, document.getElementById("node:" + node.src.data.name).childNodes[1]);
                        } else {
                            document.getElementById('olm-tree-indented-bloc-' + node.parent).appendChild(exerciseContainer);
                        }
                    } else {
                        this.htmlRoot.appendChild(exerciseContainer);
                    }
                    caret.addEventListener('click', this._toggleCaret.bind(this, caret, exerciseContainer));
                }
            }


            caret.addEventListener('click', this._toggleCaret.bind(this, caret, children));
        }

        this._drawInfos();
    }

    private _drawInfos(): void {
        this.infos = document.createElement('div');
        this.infos.innerHTML = `<svg width="20px" height="20px" viewBox="0 0 16 16"  fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                    <path d="M5.255 5.786a.237.237 0 0 0 .241.247h.825c.138 0 .248-.113.266-.25.09-.656.54-1.134 1.342-1.134.686 0 1.314.343 1.314 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.003.217a.25.25 0 0 0 .25.246h.811a.25.25 0 0 0 .25-.25v-.105c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.267 0-2.655.59-2.75 2.286zm1.557 5.763c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z"/>
                                </svg>`;
        this.infos.setAttribute('style', 'position: absolute; top: 0; left : 0; cursor: help; color:' + this.fontColor);

        let infoPanel = document.createElement('div');
        infoPanel.innerHTML = `<h4> Informations </h4>
                                <hr style="background-color:${this.fontColor} "/>
                                <p><b>${this.i18n[this.language].information.mastery.name}</b>:<br/>${this.i18n[this.language].information.mastery.description}</p>
                                <p><b>${this.i18n[this.language].information.trust.name}</b>:<br/>${this.i18n[this.language].information.trust.description}</p>
                                <p><b>${this.i18n[this.language].information.cover.name}</b>:<br/>${this.i18n[this.language].information.cover.description}</p>`;

        let infoPanelStyle: string = `border-radius:    15px; 
                                        opacity:          .95; 
                                        position:         absolute; 
                                        padding:          30px; 
                                        top:              0; 
                                        left:             0; 
                                        color:            ${this.fontColor};
                                        background-color: ${this.backgroundColor}; 
                                        width:            ${this.elem.offsetWidth.toString()}px; 
                                        height:           ${this.elem.offsetHeight.toString()}px; 
                                        display:          none`;

        infoPanel.setAttribute('style', infoPanelStyle);
        this.elem.appendChild(infoPanel);
        this.elem.appendChild(this.infos);
        this.infos.addEventListener('mouseenter', () => {
            infoPanel.style.display = 'block';
        });
        this.infos.addEventListener('mouseleave', () => {
            infoPanel.style.display = 'none';
        });
    }

    private _buildHierarchy(): Array<TreeIndentedNode> {
        let root: FrameworkTreeNode = this.framework.getRoot();
        let nodes: Array<FrameworkTreeNode> = [];
        this.framework.traverseDeepFirst((node: FrameworkTreeNode) => {
            nodes.push(node);
        });
        nodes = nodes.reverse();

        let frameworkTree: Array<TreeIndentedNode> = []
        nodes.forEach((node: FrameworkTreeNode) => {
            let treeNode: TreeIndentedNode = {
                "id": node.id,
                "parent": (node.id === root.id) ? undefined : node.parent,
                "src": node
            }
            frameworkTree.push(treeNode);
        });
        return frameworkTree;
    }

    // Event handlers ----------------------------------------------------------
    private _handleContentMouseEnter(content: Array<HTMLElement>, node: TreeIndentedNode, evt: Event): void {
        content.forEach(element => {
            element.style.color = this.fontHoverColor;
            element.style.fontWeight = 'bold';
        });
        if (this.onMouseEnter !== null) this.onMouseEnter(node.src);
    }

    private _handleContentMouseLeave(content: Array<HTMLElement>, node: TreeIndentedNode, evt: Event): void {
        content.forEach(element => {
            element.style.color = this.fontColor;
            element.style.fontWeight = 'normal';
        });
        if (this.onMouseLeave !== null) this.onMouseLeave(node.src);
    }

    private _handleContentClick(node: TreeIndentedNode, evt: Event): void {
        if (this.onClick !== null) this.onClick(node.src);
    }

    private _handleExerciseClick(exercise: HTMLElement, evt: Event): void {
        if (this.onExerciseClick !== null) this.onExerciseClick(exercise);
    }

    _handleContentVisuClick(node: TreeIndentedNode, evt: Event): void {
        if (this.onVisuCLick !== null) this.onVisuCLick(node.src);
    }

    _handleContentElemClick(node: TreeIndentedNode, type: string, evt: Event): void {
        if (this.onElemClick !== null) this.onElemClick(node.src, type);
    }

    _handleContentColumnClick(elem: HTMLElement, type: string, evt: Event): void {
        if (this.onColumnClick !== null) this.onColumnClick(elem, type);
    }

    private _handleContentMouseOver(node: TreeIndentedNode, evt: Event): void {
        if (this.onMouseOver !== null) this.onMouseOver(node.src);
    }

    private _handleContentDbClick(node: TreeIndentedNode, evt: Event): void {
        if (this.onDbClick !== null) this.onDbClick(node.src);
    }

    private _toggleCaret(caret: HTMLElement, children: HTMLElement, evt: Event) {
        if (children.style.display === 'none') {
            console.log(children);
            if (children.children[1].id.indexOf("exercise") > -1) {
                children.style.display = 'flex';
            } else {


                children.style.display = "block";
            }
            //caret.innerHTML = "<i style=\"font-size:24px\" class=\"fa\">&#xf107;</i>"
            caret.innerHTML = "&#8744;";
            caret.setAttribute('style', this._sCaretDown);
        } else {
            children.style.display = 'none';
            //caret.innerHTML = "<i style=\"font-size:24px\" class=\"fa\">&#xf105;</i>"
            caret.innerHTML = "&gt;";
            caret.setAttribute('style', this._sCaretRight);
        }
    }

    private _createScoreElement(node: TreeIndentedNode): HTMLElement {
        let color = 'rgba(0,0,0,0)';
        let cover = 0;
        let border = 'solid 1px #808080';
        if (node.src.data.mastery === undefined) color = 'rgba(0,0,0,0)';
        else if (node.src.children !== undefined && ((node.src.children.length !== 0 && node.src.data.cover > 0) || node.src.data.trust > 0)) {
            let i = 0;
            color = this.colors[i].color;
            cover = (node.src.data.cover * 100);
            if (node.src.data.cover === undefined) cover = 100;
            if (cover > 0 && cover < 12.5) {
                cover = 12.5;
            }

            while (i < this.colors.length - 1 && node.src.data.mastery > this.colors[i].to) {
                i++;
                color = this.colors[i].color;
            }
            border = 'none';
        }

        let score = document.createElement('span');
        let style = `display: inline-block; 
                     width: 21px; 
                     height: 21px; 
                     border-radius : 100%;
                     background: conic-gradient(${color} ${cover}%, transparent 0);;
                     margin-left: 5px;
                     vertical-align: text-top;
                     border: ${border};
                     box-sizing: border-box;`
        score.setAttribute('style', style);
        let htmlTooltip = `
                <p><b>${node.src.data.name}</b></p>
                <p><b>Maîtrise</b>:<br>Estime ton niveau de maîtrise pour chaque notion. Valeur : ${node.src.data.mastery != null ? (node.src.data.mastery * 100).toFixed() + '%' : 'non calculé'}</p>
                <p><b>Confiance</b>:<br>Indique la confiance dans le calcul du taux de maîtrise. Valeur : ${node.src.data.trust != null ? (node.src.data.trust * 100).toFixed() + '%' : 'non calculé'}</p>
                <p><b>Couverture</b>:<br>Indique le pourcentage des sous-notions travaillées. Valeur : ${node.src.data.cover != null ? (node.src.data.cover * 100).toFixed() + '%' : 'non calculé'}</p>
                `;
        let tooltip = document.createElement('div');
        tooltip.innerHTML = htmlTooltip;
        tooltip.style.width = '800px';
        tooltip.style.backgroundColor = this.backgroundColor;
        tooltip.style.color = this.fontColor;
        tooltip.style.position = 'relative';
        tooltip.style.display = "none";
        tooltip.style.marginTop = '2em';
        tooltip.style.marginLeft = '2em';
        tooltip.style.border = `1px solid ${this.fontColor}`;
        tooltip.style.borderRadius = '1em';
        tooltip.style.padding = '1em';
        score.addEventListener("mouseenter", () => {
            tooltip.style.display = "block";
        });
        score.addEventListener("mouseleave", () => {
            tooltip.style.display = "none";
        });
        score.appendChild(tooltip);
        if (this.onVisuCLick !== null) score.style.cursor = 'pointer';
        if (this.onVisuCLick !== null) score.addEventListener('click', this._handleContentVisuClick.bind(this, node));
        return score;
    }

    private _createContentElement(node: TreeIndentedNode): HTMLElement {
        let content = document.createElement('span');
        content.textContent = node.src.data.name;
        content.id = "node:" + node.src.data.type + ":" + node.src.data.name;
        content.style.paddingLeft = "5px";
        if (node.src.data.children !== undefined && node.src.data.children.length === 0 && node.src.data.trust === 0)
            content.style.opacity = '.3';
        return content;
    }

    private _createStatElement(text: string): HTMLElement {
        let stat = document.createElement('span');
        stat.setAttribute('style', 'float: right; width: 100px; text-align: right; fontFamily: "Courier New", Courier, monospace');
        stat.textContent = text;
        return stat;
    }

    private _createChildrenContainerElement(nodeId: string): HTMLElement {
        let children = document.createElement('div');
        children.id = 'olm-tree-indented-bloc-' + nodeId;
        if (this.rgba2hex(this.backgroundColor) >= "7d7d7d") {
            children.style.borderLeft = "dashed 1px #808080";
        } else {
            children.style.borderLeft = "dashed 1px rgba(255, 255, 255, .3)";
        }
        children.style.marginLeft = "20px";
        return children;
    }

    // HTMLElements creation ---------------------------------------------------
    private _createHTMLRoot(): void {
        this.htmlRoot = document.createElement('div');
        this.htmlRoot.setAttribute("style", "width: max-content; max-height: 800px; border: solid 1px rgba(0,0,0,0);");
        this.elem.appendChild(this.htmlRoot);
    }

    private _drawHeader(): void {
        let _createHeader = (category: string): HTMLElement => {
            let elem = document.createElement('span');
            switch (category) {
                case 'mastery':
                    elem.textContent = this.i18n[this.language].headers.mastery;
                    break;
                case 'trust':
                    elem.textContent = this.i18n[this.language].headers.trust;
                    break;
                case 'cover':
                    elem.textContent = this.i18n[this.language].headers.cover;
                    break;
            }
            elem.setAttribute("style", "width: 100px; float: right; text-align: right; color: " + this.fontColor + "; margin-right: -3pt;");
            if (this.onColumnClick !== null) elem.style.cursor = 'pointer';
            if (this.onColumnClick !== null) elem.addEventListener('click', this._handleContentColumnClick.bind(this, elem, category));
            return elem;
        }
        let titles: HTMLElement = document.createElement('div');
        titles.setAttribute("style", "width: 100%; height: 1.5em; border-image : linear-gradient(rgba(255, 255, 255, 0), rgba(255, 255, 255, .85) 1 100%;");
        if (this.showCover) titles.appendChild(_createHeader('cover'));
        if (this.showTrust) titles.appendChild(_createHeader('trust'));
        if (this.showMastery) titles.appendChild(_createHeader('mastery'));

        let borderBottom = document.createElement('div');
        borderBottom.setAttribute('style', `
        background: linear-gradient(to right, rgba(255, 255, 255, 0), rgba(255, 255, 255, .85));
        display: block;
        height:1px;
        width: 100%;
        float: right;`)
        titles.appendChild(borderBottom);
        this.htmlRoot.appendChild(titles);
    }

    private _createCaret(node: TreeIndentedNode): HTMLElement {
        let caret = document.createElement('span');
        if (node.src.children.length > 0 || node.src.resources.length > 0) {
            //caret.innerHTML = "<i style=\"font-size:24px\" class=\"fa\">&#xf107;</i>"
            caret.innerHTML = "&#8744;";
            caret.setAttribute('style', this._sCaretDown);
        } else caret.setAttribute('style', this._sNoCaret);
        return caret;
    }

    // Utilities ---------------------------------------------------------------
    private _formatValue(type: string, value: number, showable: boolean = true): string {
        if (isNaN(value) || value === undefined || value === null) return '';
        if (!showable) return '--';
        switch (type) {
            case "mastery":
                switch (this.formatMastery) {
                    case '1decimal':
                        return (Math.round(value * 10) / 10).toString();
                    case '2decimal':
                        return (Math.round(value * 100) / 100).toString();
                    case 'percentage':
                        return Math.round(value * 100).toString() + '%';
                }
                break;
            case "cover":
                switch (this.formatCover) {
                    case 'percentage':
                        return Math.round(value * 100).toString() + '%';
                }
                break;
            case "trust":
                switch (this.formatTrust) {
                    case 'percentage':
                        return Math.round(value * 100).toString() + '%';
                }
                break;
        }
        return 'a';
    }
}
